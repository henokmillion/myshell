/**
* myshell.c
* 
* Distributed Systems Programming - Lab 2
* Process and Exec
*
* Henok Million (ATR/1810/08)
* SE - section 2
* December 2018
*/


#include <stdio.h>
#include <sys/types.h>
#include <string.h>
#include <stdlib.h>

void parse(char *line, char *arg[]);
void execute(char *arg[]);

/**
* main
* @params:
* @return:
*/
void main(void)
{
	char line[1024];
	char *argv[64];
	while(1) {
		printf("\nShell -> ");
		gets(line); // get string from stdin
		// parse/tokenize input into commands
		//  and arguments
		parse(line, argv);
		// skip empty line
		if (strlen(line) == 0) {
			continue;
		}
		if (strcmp(argv[0], "exit") == 0)
		{
			exit(0);
		}
		// execute command
		execute(argv);
	}
}

/**
* parse
*
* tokenizes line command 
* by whitespae
*
* @param: line: char *
* @param: argv: char* []
* @return: 
*/
void parse(char *line, char *argv[])
{
    int argc = 0;
    char *token;
    token = strtok(line, " "); // first argument
    // tokenize each argument and save to argv array
    while (token != NULL) {
        argv[argc] = token;
        token = strtok(NULL, " ");
        argc++;
    }
    argv[argc] = NULL;
}

/**
* execute
* @param: argv: char* []
* @return:
*/
void execute(char *argv[])
{
	if (fork() == 0) {
		execvp(argv[0], argv);
		printf("\n");
	} else {
		wait(NULL);
	}
}